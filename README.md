



![Phoca Download](phocadownload.png)

# Phoca Download



Phoca Download is download manager for Joomla! CMS. It includes component, modules and plugins and allows displaying files on website which can be downloaded (previewed, played) by website visitors.



## Project page

[Phoca Download](https://www.phoca.cz/phocadownload)

[Phoca Download Info Page](https://www.phoca.cz/project/phocadownload-joomla-download)



## Documentation

[Documentation](https://www.phoca.cz/documentation/category/17-phoca-download-component)



## Support

[Phoca Forum](https://www.phoca.cz/forum)

:bell: [Phoca Forum RSS](https://www.phoca.cz/forum/app.php/feed)



## News

[Phoca News](https://www.phoca.cz/news)

:bell: [Phoca News RSS](https://www.phoca.cz/news?format=feed&type=rss)

:bell: [Phoca Latest Releases RSS](https://www.phoca.cz/download/feed/111?format=feed&type=rss)



## Demo

[Joomla! 3 Demo](https://www.phoca.cz/joomla3demo/)

[Joomla! 2.5 Demo](https://www.phoca.cz/joomlademo/)

[Joomla! 1.5 Demo](https://www.phoca.cz/demo/)



## Version

3.1.7



## License

GNU/GPL



This project is open source project - feel free to contribute! Thank you.
